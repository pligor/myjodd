package com.pligor.myjodd

import jodd.mail._
import scala.collection.JavaConversions._

import javax.mail.internet.{AddressException, InternetAddress}

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 * https://github.com/oblac/jodd
 */
object EmailHelper {
  val emailsRelativePath = "private/emails";

  val embeddedImageLinkPrefix = "cid:";

  def sendSingleMail(email: Email, smtpServer: SmtpServer) {
    val session = smtpServer.createSession();
    try {
      session.open();
      session.sendMail(email);
    } finally {
      session.close();
    }
  }

  def receivedEmail2Email(receivedEmail: ReceivedEmail): Email = {
    //"from" is us
    //"to" is them
    val email = Email.create().
      subject(receivedEmail.getSubject).
      priority(receivedEmail.getPriority);

    val messages = receivedEmail.getAllMessages;

    for (message <- messages) {
      email.addMessage(message);
    }

    val attachments = email.getAttachments;
    if (attachments != null) {
      for (attachment <- attachments) {
        email.attach(attachment);
      }
    }

    email;
  }

  def isValidEmailAddress(email: String): Boolean = {
    try {
      new InternetAddress(email).validate();
      true;
    } catch {
      case ex: AddressException => false;
    }
  }
}
