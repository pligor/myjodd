A helper class to make even simpler jodd email api

### Dependencies ###
"org.jodd" % "jodd-mail" % "3.4.10"

Used in projects:

* [bman](http://bman.co)
* [Fcarrier](http://facebook.com/FcarrierApp)